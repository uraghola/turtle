Проект UnrealEngine
Тестовое задание с черепашками

При открытии появляется сцена с тремя кубами-кнопками и одним управляемым шаром. Шар активируется при нажатии кнопки мыши и тогда им можно двигать на w,a,s,d. 
При прикосновении шара с кубами те пропадают и появляются черепашки, которые идут к финишу.
